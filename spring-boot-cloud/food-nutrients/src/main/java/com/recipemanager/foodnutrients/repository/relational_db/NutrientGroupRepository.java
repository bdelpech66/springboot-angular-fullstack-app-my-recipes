package com.recipemanager.foodnutrients.repository.relational_db;

import com.recipemanager.foodnutrients.entity.NutrientGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NutrientGroupRepository extends JpaRepository<NutrientGroup, Long> {
}