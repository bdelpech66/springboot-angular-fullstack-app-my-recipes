package com.recipemanager.foodnutrients;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableJpaRepositories(basePackages = "com.recipemanager.foodnutrients.repository.relational_db")
//@EnableMongoRepositories(basePackages = "com.recipemanager.foodnutrients.repository.mongodb")
//@Import(GeneralSecurityConfiguration.class)
@SpringBootApplication
public class FoodNutrientsApplication {
	public static void main(String[] args) {
		SpringApplication.run(FoodNutrientsApplication.class, args);
	}
}
