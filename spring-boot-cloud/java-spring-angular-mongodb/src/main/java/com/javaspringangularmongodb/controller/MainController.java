package com.javaspringangularmongodb.controller;


import com.javaspringangularmongodb.entity.Question;
import com.javaspringangularmongodb.services.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RequestMapping("/questions")
@RestController
public class MainController {
    private QuestionService questionService;

    @GetMapping
    public ResponseEntity<List<Question>> questions() {
        return new ResponseEntity<>(
                questionService.getAllQuestions(),
                HttpStatus.OK
        );
    }

    @PostMapping
    public ResponseEntity<Question> createQuestion(@RequestBody Question question) {
        return new ResponseEntity<>(
                questionService.save(question),
                HttpStatus.CREATED
        );
    }
}