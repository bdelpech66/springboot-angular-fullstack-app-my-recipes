package com.javaspringangularmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

//@Import(GeneralSecurityConfiguration.class)
@SpringBootApplication()
public class JavaSpringAngularMongoDbApplication {

    public static void main(String[] args) {
        Map<String, String> envVars = System.getenv();
        for (Map.Entry<String, String> entry : envVars.entrySet()) {
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
        SpringApplication.run(JavaSpringAngularMongoDbApplication.class, args);
    }

}
