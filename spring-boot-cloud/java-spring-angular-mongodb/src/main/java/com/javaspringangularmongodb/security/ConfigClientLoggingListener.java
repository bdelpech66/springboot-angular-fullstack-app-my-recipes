package com.javaspringangularmongodb.security;

import org.springframework.cloud.endpoint.event.RefreshEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;

@Configuration
public class ConfigClientLoggingListener implements ApplicationListener<RefreshEvent> {

    private final ConfigurableEnvironment environment;

    public ConfigClientLoggingListener(ConfigurableEnvironment environment) {
        this.environment = environment;
    }

    @Override
    public void onApplicationEvent(RefreshEvent event) {
        MutablePropertySources propertySources = environment.getPropertySources();
        for (PropertySource<?> propertySource : propertySources) {
            // Log the property source, you can also log individual properties if needed
            System.out.println("Property Source: " + propertySource.getName());
        }
    }
}