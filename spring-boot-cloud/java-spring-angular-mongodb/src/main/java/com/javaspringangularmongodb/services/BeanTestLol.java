package com.javaspringangularmongodb.services;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.*;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.StringValueResolver;
import org.springframework.web.context.ServletConfigAware;
import org.springframework.web.context.ServletContextAware;

import java.util.List;


@Component
public class BeanTestLol implements
        BeanNameAware,
        BeanFactoryAware,
        ApplicationContextAware,
        ApplicationEventPublisherAware,
        BeanClassLoaderAware,
        DisposableBean,
        EmbeddedValueResolverAware,
        EnvironmentAware,
        InitializingBean,
        MessageSourceAware,
        ResourceLoaderAware,
        ServletConfigAware,
        ServletContextAware {


    private String beanName;
    private ApplicationContext applicationContext;


    // BeanNameAware
    @Override
    public void setBeanName(String name) {
        this.beanName = name;
        // Called when the bean is instantiated and added to the bean factory.
        System.out.println("Bean name is: " + name);
    }

    @Override
    public void setBeanClassLoader(ClassLoader classLoader) {
        // Called after the bean is fully created but before any post-processing.
        System.out.println("Classloader: " + classLoader.toString());
    }


    @Override
    public void setEmbeddedValueResolver(StringValueResolver resolver) {
        // Called during bean post-processing, allowing the bean to resolve string values.
        String resolvedString = resolver.resolveStringValue("Hello ${user.name}");
        System.out.println("Resolved string value: " + resolvedString);
    }


    @Override
    public void setEnvironment(Environment environment) {
        // Called after the bean is fully created, allowing the bean to query environment properties.
        String activeProfile = List.of(environment.getActiveProfiles()).isEmpty() ? environment.getDefaultProfiles()[0] : environment.getActiveProfiles()[0];
        System.out.println("Active profile: " + activeProfile);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        // Called after the bean is added to the bean factory but before any post-processing.
        System.out.println("Is singleton: " + beanFactory.isSingleton(beanName));
    }


    // BeanFactoryAware

    // ApplicationContextAware
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        // Called after the bean factory is aware of the application context.
        System.out.println("Application context initialized.");
    }

    // ApplicationEventPublisherAware
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        // Called after the application context is set, allowing the bean to publish events.
        applicationEventPublisher.publishEvent(new MyEvent(this, "Event message"));
    }

    // BeanClassLoaderAware


    // DisposableBean
    @Override
    public void destroy() throws Exception {
        // Called when the bean is being destroyed, during the shutdown of the application context.
        System.out.println("Bean is being destroyed.");
    }

    // EmbeddedValueResolverAware

    // EnvironmentAware


    // InitializingBean
    @Override
    public void afterPropertiesSet() throws Exception {
        // Called after all properties are set, but before the bean is ready for use.
        System.out.println("Bean properties have been set.");
    }

    // MessageSourceAware
    @Override
    public void setMessageSource(MessageSource messageSource) {
        // Called after the application context is set, allowing the bean to resolve messages.
        String message = messageSource.getMessage("some.key", null, "Default", null);
        System.out.println("Message: " + message);
    }

    // ResourceLoaderAware
    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        // Called after the application context is set, allowing the bean to load resources.
        System.out.println("Resource loader: " + resourceLoader.getClass().getName());
    }

    // ServletConfigAware
    @Override
    public void setServletConfig(ServletConfig servletConfig) {
        // Called after the bean is fully created, if running in a web application context.
        System.out.println("Servlet name: " + servletConfig.getServletName());
    }

    // ServletContextAware
    @Override
    public void setServletContext(ServletContext servletContext) {
        // Called after the bean is fully created, if running in a web application context.
        System.out.println("Servlet context name: " + servletContext.getServletContextName());
    }


    class MyEvent extends ApplicationEvent {
        private final String message;

        public MyEvent(Object source, String message) {
            super(source);
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

}
