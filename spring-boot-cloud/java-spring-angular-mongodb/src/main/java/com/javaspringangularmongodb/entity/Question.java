package com.javaspringangularmongodb.entity;

import com.edelpech.sharedlibrarystarter.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "question")
public class Question extends BaseEntity {

    private String question;
    private String answer;
}
