package com.javaspringangularmongodb.repository;

import com.javaspringangularmongodb.entity.RecipeFood;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeFoodRepository extends JpaRepository<RecipeFood, Long> {
}