package com.javaspringangularmongodb.entity;

import com.edelpech.sharedlibrarystarter.BaseEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "recipe")
public class Recipe extends BaseEntity {
    String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipe")
    private Set<RecipeFood> recipeFoods = new HashSet<>();

    public void addRecipeFood(RecipeFood recipeFood) {
        recipeFood.setRecipe(this);
        this.recipeFoods.add(
                recipeFood
        );
    }
}
