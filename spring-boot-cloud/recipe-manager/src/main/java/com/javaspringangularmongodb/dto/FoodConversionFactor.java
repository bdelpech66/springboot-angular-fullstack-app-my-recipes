package com.javaspringangularmongodb.dto;

public record FoodConversionFactor(Long foodId, Float conversionFactor) {
}
