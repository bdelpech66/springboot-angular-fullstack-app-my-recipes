package com.javaspringangularmongodb.dto;

public record FoodMinimal(Long id,String name, String nameFrench) {
}
