package com.javaspringangularmongodb.dto;

public record ConversionFactorMinimal(Long id, Float factor, MeasureMinimal measure) {
}
