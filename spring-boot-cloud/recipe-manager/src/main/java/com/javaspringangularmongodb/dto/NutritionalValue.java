package com.javaspringangularmongodb.dto;

public record NutritionalValue(
        Long foodId,
        Nutrient nutrient,
        Float value
) {
}
