package com.javaspringangularmongodb.dto;

public record MeasureMinimal(Long id, String name, String nameFrench) {
}
