package com.javaspringangularmongodb.dto;

public record RecipeFoodDto(
        Long id,
        FoodMinimal food,
        ConversionFactorMinimal conversionFactor,
        Float quantity

) {
}
